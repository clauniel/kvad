import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import time
import serial
from matplotlib.widgets import CheckButtons


plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)
xar = []
yar = []

#history
accX = []
accY = []
accZ = []
gyroX = []
gyroY = []
gyroZ = []
magX = []
magY = []
magZ = []
temp = []
rot0 = []
rot1 = []
rot2 = []
rot3 = []
vect0 = []
vect1 = []
vect2 = []
heading = []
elevation = []


#plot animation function 
#def animate(i):
#    pullData = open("sampleText.txt","r").read()
#    dataArray = pullData.split('\n')
#    for eachLine in dataArray:
#        if len(eachLine)>1:
#            x,y = eachLine.split(',')
#            xar.append(int(x))
#            yar.append(int(y))
#    ax1.clear()
#    ax1.plot(xar,yar)

def make_plot():
    ax1.clear()
    #ax1.plot(xar,accX)
    #ax1.plot(xar,accY)
    #ax1.plot(xar,accZ)
    #ax1.plot(xar,gyroX)
    #ax1.plot(xar,gyroY)
    #ax1.plot(xar,gyroZ)
    #ax1.plot(xar,magX)
    #ax1.plot(xar,magY)
    #ax1.plot(xar,magZ)
    #ax1.plot(xar,rot0)
    #ax1.plot(xar,rot1)
    #ax1.plot(xar,rot2)
    #ax1.plot(xar,rot3)
    #ax1.plot(xar,vect0)
    #ax1.plot(xar,vect1)
    #ax1.plot(xar,vect2)
    #ax1.plot(xar,heading)
    ax1.plot(xar,elevation)
    ax1.set_xlim([xar[0],xar[len(xar)-1]])
    plt.draw()

#Check buttons
rax = plt.axes([0.05, 0.4, 0.1, 0.15])
check = CheckButtons(rax, ('2 Hz', '4 Hz', '6 Hz'), (False, True, True))
#What to do when checked
def updateGraphs(label):
    if label == '2 Hz': ax1.set_visible(not ax1.get_visible())
    elif label == '4 Hz': ax1.set_visible(not ax1.get_visible())
    elif label == '6 Hz': ax1.set_visible(not ax1.get_visible())
    plt.draw()
#assign event handler
check.on_clicked(updateGraphs)

#open serial port
ser = serial.Serial( port='/dev/ttyACM3', baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0 );

#start animation
#ani = animation.FuncAnimation(fig, animate, interval=1000)
#show figure

segment = []
curData = []
segmentCount = 0;
#read from serial port
#ser = open('./sampleSerial.txt')
while True:
    try:
        for c in ser.read():
            c = str(chr(c))
            segment.append( c )
            if c == '\033':
                ser.read()
                break
    except SerialException:
        pass
    try:
        for c in ser.read():
            c = str(chr(c))
            segment.append( c )
            if c == '\033':
                print( ''.join( segment ), end='' )
                segment = ''.join(segment).replace(',','').split('\n')
                for line in segment:
                    line = line.split()
                    if line:
                        l = []
                        for t in line:
                            try:
                                l.append(float(t))
                            except ValueError:
                                pass
                        if l:
                            curData.append( l )

                try:
                    if segmentCount > 200:
                        accX.pop(0)
                        accY.pop(0)
                        accZ.pop(0)
                        gyroX.pop(0)
                        gyroY.pop(0)
                        gyroZ.pop(0)
                        magX.pop(0)
                        magY.pop(0)
                        magZ.pop(0)
                        temp.pop(0)
                        rot0.pop(0)
                        rot1.pop(0)
                        rot2.pop(0)
                        rot3.pop(0)
                        vect0.pop(0)
                        vect1.pop(0)
                        vect2.pop(0)
                        heading.pop(0)
                        elevation.pop(0)
                        xar.pop(0)

                    accX.append( curData[0][0] )
                    accY.append( curData[0][1] )
                    accZ.append( curData[0][2] )
                    gyroX.append( curData[1][0] )
                    gyroY.append( curData[1][1] )
                    gyroZ.append( curData[1][2] )
                    magX.append( curData[2][0] )
                    magY.append( curData[2][1] )
                    magZ.append( curData[2][2] )
                    temp.append( curData[3][0] )
                    rot0.append( curData[4][0] )
                    rot1.append( curData[4][1] )
                    rot2.append( curData[4][2] )
                    rot3.append( curData[4][3] )
                    vect0.append( curData[5][0] )
                    vect1.append( curData[5][1] )
                    vect2.append( curData[5][2] )
                    heading.append( curData[6][0] )
                    elevation.append( curData[7][0] )

                    segment = []
                    #print(curData)
                    #print( curData )
                    curData = []
                    xar.append( segmentCount )
                    yar.append( segmentCount )
                    segmentCount = segmentCount + 1
                    if segmentCount % 5 == 0:
                        make_plot()
                except:
                    #print('except')
                    segment = []
                    curData = []
    except SerialException:
        pass
        
