%% Server receiving svo data and live plotting
% start server
tcpipServer = tcpip('0.0.0.0',55000,'NetworkRole','Server');
set(tcpipServer, 'inputbuffersize', 2^15 ) %32 KB buffer because why not
%% Main loop
fopen(tcpipServer)                          %wait for connection
lHandle = plot3(nan, nan, nan);             %get line handle
view([0,90]);                               %set 2d view
xlim([-1,1]);                              %set xlimit
ylim([-1,1]);                               %set ylimit
while 1                                     %keep going until 'stopping'
    rawData = fgetl(tcpipServer);           %read data from buffer
    if strcmp(rawData, 'stopping' )         %check for exit flag
        break;
    end
    xyz = eval( [ '[', rawData, ']' ] );    %convert to doubles
    X = get( lHandle, 'XData' );            %get X data in plot
    Y = get( lHandle, 'YData' );            %get Y data in plot
    Z = get( lHandle, 'ZData' );            %get Z data in plot
    
    X = [X xyz(1)];                         %update X data
    Y = [Y xyz(2)];                         %update Y data
    Z = [Z xyz(3)];                         %update Z data
    
    set( lHandle, 'XData', X, 'YData', Y, 'ZData', Z ); %notify plot of update
    drawnow;                                %force plot update to animate
end
fclose(tcpipServer)                         %close connection
