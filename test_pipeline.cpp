// This file is part of SVO - Semi-direct Visual Odometry.
//
// Copyright (C) 2014 Christian Forster <forster at ifi dot uzh dot ch>
// (Robotics and Perception Group, University of Zurich, Switzerland).
//
// SVO is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// SVO is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <svo/config.h>
#include <svo/frame_handler_mono.h>
#include <svo/map.h>
#include <svo/frame.h>
#include <vector>
#include <string>
#include <vikit/math_utils.h>
#include <vikit/vision.h>
#include <vikit/abstract_camera.h>
#include <vikit/atan_camera.h>
#include <vikit/pinhole_camera.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sophus/se3.h>
#include <iostream>
#include "test_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

namespace svo {

class BenchmarkNode
{
  vk::AbstractCamera* cam_;
  svo::FrameHandlerMono* vo_;

public:
  BenchmarkNode();
  ~BenchmarkNode();
  void runFromFolder(int sockfd);
};

BenchmarkNode::BenchmarkNode()
{
  //cam_ = new vk::PinholeCamera(752, 480, 315.5, 315.5, 376.0, 240.0);
  cam_ = new vk::PinholeCamera(640, 480, 532.2419, 528.8905, 312.3045, 235.0858, -0.0946, 0.1951);
  vo_ = new svo::FrameHandlerMono(cam_);
  vo_->start();
}

BenchmarkNode::~BenchmarkNode()
{
  delete vo_;
  delete cam_;
}

void BenchmarkNode::runFromFolder(int sockfd)
{
  FILE *f = fopen( "coords.csv", "w+" );
  char buffer[256];
  cv::VideoCapture cap(-1);
  //for(int img_id = 2; img_id < 188; ++img_id)
  int img_id = 2;
  while ( cap.isOpened() )
  {
	bzero( buffer, 256 );
    // load image
    //std::stringstream ss;
    //ss << svo::test_utils::getDatasetDir() << "/sin2_tex2_h1_v8_d/img/frame_"
    //   << std::setw( 6 ) << std::setfill( '0' ) << img_id << "_0.png";
    //if(img_id == 2)
    //  std::cout << "reading image " << ss.str() << std::endl;
    //cv::Mat img(cv::imread(ss.str().c_str(), 0));
	cv::Mat img;
	if ( ! cap.read(img) )
		break;
	cv::cvtColor( img, img, CV_BGR2GRAY );
    assert(!img.empty());

    // process frame
    vo_->addImage(img, 0.01*img_id);

    // display tracking quality
    if(vo_->lastFrame() != NULL)
    {
    	std::cout << "Frame-Id: " << vo_->lastFrame()->id_ << " \t"
                  << "#Features: " << vo_->lastNumObservations() << " \t"
                  << "Proc. Time: " << vo_->lastProcessingTime()*1000 << "ms\n";

		snprintf( buffer, 256, "%f, %f, %f\n", vo_->lastFrame()->T_f_w_.translation()(0), vo_->lastFrame()->T_f_w_.translation()(1),
				vo_->lastFrame()->T_f_w_.translation()(2) );
		write( sockfd, buffer, strlen(buffer) );
    	// access the pose of the camera via vo_->lastFrame()->T_f_w_.
    }
	img_id++;
  }
  write( sockfd, "stopping\n", strlen("stopping\n") );
  fclose( f );
  close( sockfd );
}

} // namespace svo

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int sockfd, portno, n;

int main(int argc, char** argv)
{
    struct sockaddr_in serv_addr;
    struct hostent *server;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
  {
    svo::BenchmarkNode benchmark;
    benchmark.runFromFolder(sockfd);
  }
  printf("BenchmarkNode finished.\n");
  return 0;
}

